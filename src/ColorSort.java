public class ColorSort {

   enum Color {red, green, blue};

   public static void main (String[] param) {
      // for debugging
   }

   public static void reorder (Color[] balls) {
      int red = 0; // Punaste pallide counter
      int green = 0; // Roheliste pallide counter
      int blue = 0; // Siniste pallide counter
      int location = 0; // Positsioon counter

      for (Color ball : balls) {
         if (ball.equals(Color.red)) {
            red++; // Kui esineb massivis punane pall, siis suurendame punase palli counterit ühe võrra.
         } else if (ball.equals(Color.green)) {
            green++; // Kui esineb massivis roheline pall, siis suurendame rohelise palli counterit ühe võrra.
         } else if (ball.equals(Color.blue)) {
            blue++; // Kui esineb massivis sinine pall, siis suurendame sinise palli counterit ühe võrra.
         }
      }

      for (int i = 0; i < red; i++) {
         balls[location] = Color.red; // Lisame kõik loendatud punased pallid massivi algusesse.
         location ++; // Suurendame positsiooni counterit ühe võrra.
      }

      for (int i = 0; i < green; i++) {
         balls[location] = Color.green; // Lisame kõik lendatud rohelised pallid peale punaseid palle.
         location ++; // Suurendame positsiooni counterit ühe võrra.
      }

      for (int i = 0; i < blue; i++) {
         balls[location] = Color.blue; // Lisame kõik lendatud sinised pallid peale rohelisi palle.
         location ++; // Suurendame positsiooni counterit ühe võrra.
      }
   }
}
